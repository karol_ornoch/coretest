﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreTest.Models
{
    public class Book
    {
        public int ID { get; set; }
        [Display(Name ="Tytuł")]
        public string Title { get; set; }
        [Display(Name = "Autor")]
        public string Author { get; set; }
        public string ISBN { get; set; }
        [Display(Name = "Data publikacji")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PublicationDate { get; set; }

    }
}
