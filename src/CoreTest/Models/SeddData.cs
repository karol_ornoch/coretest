﻿using CoreTest.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTest.Models
{
    public class SeddData

    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
               serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                if (context.Book.Any())
                {
                    return;
                }

                context.Book.AddRange(
                    new Book
                    {
                        Author = "Jakis Autror",
                        ISBN = "12-3424-123-123",
                        PublicationDate = new DateTime(2014, 5, 3),
                        Title = "Tytuł"
                    }
                );
                context.SaveChanges();
            }

        }

    }
}
